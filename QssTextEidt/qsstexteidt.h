#ifndef QSSTEXTEIDT_H
#define QSSTEXTEIDT_H

#include <QWidget>
#include <QCompleter>
#include <QTextCursor>
namespace Ui {
class QssTextEidt;
}


class QssTextEidt : public QWidget
{
    Q_OBJECT

public:
    explicit QssTextEidt(QWidget *parent = nullptr);
    ~QssTextEidt();
signals:
    void SetStyleSheet(QString styleSheet);
protected:
    bool eventFilter(QObject* watched,QEvent* event);
private:
    void OnEdit();
private:
    bool IsValidStyleSheet();
    void SetCompleter();
    QString GetWord();
private slots:
    void OnCompleterActivated(QString word);
        void OnHighLighted(QString);
private:
    Ui::QssTextEidt *ui;
    QCompleter* m_completer;
    QString m_currentWord;
};

#endif // QSSTEXTEIDT_H
