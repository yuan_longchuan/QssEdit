#include "qsstexteidt.h"
#include "ui_qsstexteidt.h"
#include <QStyle>
#include <QDebug>
#include <QFile>
#include <QRegExpValidator>
#include <QKeyEvent>

QRegExp wordExp=QRegExp("((:|::|-)([a-zA-Z])?)|((-|[a-zA-Z])+)"); //匹配单词



QssTextEidt::QssTextEidt(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QssTextEidt)
{
    ui->setupUi(this);
    //ui->textEdit->installEventFilter(this);
    m_completer=nullptr;
    connect(ui->textEdit,&QTextEdit::textChanged,this,&QssTextEidt::OnEdit);
}

QssTextEidt::~QssTextEidt()
{
    delete ui;
}

void QssTextEidt::SetCompleter()
{
    if(m_completer)
        return;
    QStringList strList;
    QFile file("./default/info.data");
    if(file.open(QIODevice::ReadOnly))
    {
       strList=QString(file.readAll()).split('\n');
       for(auto var:strList)
       {
           if(var.isEmpty())
           {
               strList.removeOne(var);
           }
       }
    }
    m_completer=new QCompleter(strList);
    m_completer->setWidget(ui->textEdit);
    m_completer->setMaxVisibleItems(8);
    m_completer->setCaseSensitivity(Qt::CaseInsensitive);
    connect(m_completer, SIGNAL(activated(QString)), this, SLOT(OnCompleterActivated(QString)));
    connect(m_completer,SIGNAL(highlighted(QString)),this,SLOT(OnHighLighted(QString)));
}

bool QssTextEidt::IsValidStyleSheet()
{
    // 想增加一个判断当前stylesheet是否合法的功能
    return true;
}

QString QssTextEidt::GetWord()
{
    QTextCursor cursor=ui->textEdit->textCursor();
    QString word;
    while (cursor.movePosition(QTextCursor::Left, QTextCursor::KeepAnchor, 1))
    {
        word=cursor.selectedText();
        QRegExpValidator validator(wordExp);
        int pos=0;
        if(validator.validate(word,pos)==QValidator::State::Acceptable)
        {
            continue;
        }
        else
        {
            if(word.size()>=1)
                word.remove(0,1);
            break;
        }
    }
    return word;
}

void QssTextEidt::OnEdit()
{
    //弹出提示
    {
        m_currentWord.clear();
        m_currentWord=GetWord();
        qDebug()<<m_currentWord;
        if(!m_currentWord.isEmpty())
        {
            SetCompleter();
            m_completer->setCompletionPrefix(m_currentWord);
            QRect rect=ui->textEdit->cursorRect();
            rect.setWidth(200);
            m_completer->complete(rect);
        }
        else
        {
            //delete m_completer;
            //m_completer=nullptr;
            m_completer->deleteLater();
            m_completer=nullptr;
        }
    }

    if(IsValidStyleSheet())
     {
        emit SetStyleSheet(ui->textEdit->toPlainText().trimmed());
    }
}

void QssTextEidt::OnCompleterActivated(QString word)
{
    disconnect(ui->textEdit,&QTextEdit::textChanged,this,&QssTextEidt::OnEdit);
    QTextCursor cursor=ui->textEdit->textCursor();
    cursor.movePosition(QTextCursor::Left, QTextCursor::KeepAnchor, m_currentWord.length());
    cursor.removeSelectedText();
    cursor.insertText(word);
    //cursor.movePosition(QTextCursor::Right,QTextCursor::MoveAnchor,word.length());
    ui->textEdit->setTextCursor(cursor);
    connect(ui->textEdit,&QTextEdit::textChanged,this,&QssTextEidt::OnEdit);
}

void QssTextEidt::OnHighLighted(QString word)
{

}

bool QssTextEidt::eventFilter(QObject *watched, QEvent *event)
{
    if(watched==ui->textEdit)
    {
        if(event->type()==QEvent::KeyPress)
        {
            QKeyEvent *keyEvent=static_cast<QKeyEvent*>(event);
            if(keyEvent->key()==Qt::Key_Enter||keyEvent->key()==Qt::Key_Return)
            {
                //接受enter事件  补全
            }
        }

    }

    return QWidget::eventFilter(watched,event);
}
