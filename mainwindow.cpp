#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGridLayout>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Init();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::Init()
{
    ui->splitter->setStretchFactor(0,2);
    ui->splitter->setStretchFactor(1,6);
    ui->widget_editArea->layout()->addWidget(&m_textEidt);
    ui->scrollAreaWidgetContents->layout()->addWidget(&m_showWidget);
    //ui->widget_showArea->layout()->addWidget(&m_showWidget);
    //QGridLayout* gridLayout=qobject_cast<QGridLayout*>(ui->widget_showArea->layout());
    //gridLayout->addWidget(&m_showWidget,1,0,1,2);
    connect(ui->action_selectUi,SIGNAL(triggered()),&m_showWidget,SLOT(OpenUi()));
    connect(&m_textEidt,&QssTextEidt::SetStyleSheet,&m_showWidget,&QssShowWidget::SetStyleSheet);
}
