#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QssTextEidt/qsstexteidt.h"
#include "QssShowWidget/qssshowwidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void Init();
private:
    Ui::MainWindow *ui;
    QssTextEidt m_textEidt;
    QssShowWidget m_showWidget;
};
#endif // MAINWINDOW_H
