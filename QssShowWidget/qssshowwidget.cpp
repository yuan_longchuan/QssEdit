#include "qssshowwidget.h"
#include "ui_qssshowwidget.h"
#include <QFileDialog>
#include <QtUiTools/QUiLoader>

QssShowWidget::QssShowWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QssShowWidget)
{
    ui->setupUi(this);
}

QssShowWidget::~QssShowWidget()
{
    delete ui;
}


void QssShowWidget::OpenUi()
{
   QString fileName=QFileDialog::getOpenFileName(this,QStringLiteral("选择文件"),QDir::currentPath(),"*.ui");
   QFileInfo info(fileName);
   if(!info.exists())
       return;
   QUiLoader loader;
   QFile file(fileName);
   if(file.open(QIODevice::ReadOnly))
   {
       QWidget* widget=loader.load(&file);
       if(m_currentWidget)
       {
           ui->gridLayout->removeWidget(m_currentWidget);
           delete  m_currentWidget;
       }
       ui->gridLayout->addWidget(widget);
       m_currentWidget=widget;
   }
}


void QssShowWidget::SetStyleSheet(QString styleSheet)
{
    if(m_currentWidget)
        m_currentWidget->setStyleSheet(styleSheet);
}
