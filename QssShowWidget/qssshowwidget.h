#ifndef QSSSHOWWIDGET_H
#define QSSSHOWWIDGET_H

#include <QWidget>

namespace Ui {
class QssShowWidget;
}

class QssShowWidget : public QWidget
{
    Q_OBJECT

public:
    explicit QssShowWidget(QWidget *parent = nullptr);
    ~QssShowWidget();
public slots:
    void OpenUi();
    void SetStyleSheet(QString styleSheet);
private:
    Ui::QssShowWidget *ui;
    QWidget* m_currentWidget=nullptr;
};

#endif // QSSSHOWWIDGET_H
